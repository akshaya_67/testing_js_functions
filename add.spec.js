const {add, head_func,tail_func, max_func, min_func, map_func, filter_func, reduce_func} = require('./add');
//import {add, head_func} from './add';
describe('Add', () => {

    it('Sum of 0 and 0 is 0', () => {
        expect(add(0, 0)).toEqual(0);
    });

    it('Sum of 1 and 0 is 1', () => {
        expect(add(1, 0)).toEqual(1);
    });

})

describe('Head_func', () => {

    it('Head of [1,2,3] is 1', () => {
        expect(head_func([1,2,3])).toEqual(1);
    });

    it('Head of [] is null', () => {
        expect(head_func([])).toEqual(null);
    });

})

describe('Tail_func', () => {

    it('Tail of [1,2,3] is [2,3]', () => {
        expect(tail_func([1,2,3])).toEqual([2,3]);
    });

    it('Tail of [] is null', () => {
        expect(tail_func([])).toEqual(null);
    });

})

describe('Max_func', () => {

    it('Max of [1,2,3] is 3', () => {
        expect(max_func([1,2,3])).toEqual(3);
    });

    it('Max of [1] is 1', () => {
        expect(max_func([1])).toEqual(1);
    });

    it('Max of [] is null', () => {
        expect(max_func([])).toEqual(null);
    });

})

describe('Min_func', () => {

    it('Min of [1,2,3] is 1', () => {
        expect(min_func([1,2,3])).toEqual(1);
    });

    it('Min of [1] is 1', () => {
        expect(min_func([1])).toEqual(1);
    });

    it('Min of [] is null', () => {
        expect(min_func([])).toEqual(null);
    });

})

describe('Map_func',()=>{
    it('Map ([1,2,3], cube) is [1,8,27]', () => {
        expect(map_func([1,2,3],'cube')).toEqual([1,8,27]);
    });

    it('map([], cube) is []',()=>{
        expect(map_func([],'cube')).toEqual([]);
    });

    it('map([1,2,3], identity) is [1,2,3]',()=>{
        expect(map_func([1,2,3],'identity')).toEqual([1,2,3]);
    });
});

describe('Filter_func',()=>{

    it('Filter ([]  ,x => true) is []', () => {
        expect(filter_func([],x => true)).toEqual([]);
    });

    it('Filter ([1,2,3], x => true) is [1,2,3]', () => {
        expect(filter_func([1,2,3],x => true)).toEqual([1,2,3]);
    });

    it('Filter([1,2,3],x => false) is []', () => {
        expect(filter_func([1,2,3],x => false)).toEqual([]);
    });

    it('Filter ([1,2,3], x => x>1) is [2,3]', () => {
        expect(filter_func([1,2,3],x => x > 1)).toEqual([2,3]);
    });

    it('Filter([a,B,c,D], toUpperCase) is [B,D]',()=>{
        expect(filter_func(['a','B','c','D'], x=> x == x.toUpperCase())).toEqual(['B','D']);
    });
    
    
});

describe('Reduce_func',()=>{

    it('Reduce ([],(x,y)=>x+y) is undefined', () => {
        expect(reduce_func([],(x,y)=>x+y)).toEqual(undefined);
    });

    it('reduce ([] ,(x,y)=>x+y,10) is 10', () => {
        expect(reduce_func([],(x,y)=>x+y,10)).toEqual(10);
    });
    
    it('reduce ([a,b,c] ,(x,y)=>x+y) is abc', () => {
        expect(reduce_func(['a','b','c'],(x,y)=>x+y)).toEqual('abc');
    });

    it('reduce ([a,b,c], (x,y)=>x+y,z) is zabc', () => {
        expect(reduce_func(['a','b','c'],(x,y)=>x+y,'z')).toEqual('zabc');
    });
});