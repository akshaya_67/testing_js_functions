const add = (first,second) => {
    return first + second;   
}


const head_func = (a) => {
    if (a. length!=0) return a[0]; 
    else{
        return null;
    }
}

const tail_func = (a) => {
    if (a. length> 1) {
        r = a.splice(1);
        return r;

    }
    else{
        return null;
    }
}

const filter_func = (a, condition) => {
    return a.filter(condition);
}

const map_func = (a, fn) => {
      const cube = (number) =>{
         return Math.pow(number,3);
        };
      const identity = (number) =>{
         return number;
        }

    return a.map(eval(fn));

}

const reduce_func = (a,previousValue, initialValue = undefined) =>{
    if(initialValue == undefined) {
        if(a.length == 0){
            return undefined;
        }
        else{
            return a.reduce(previousValue);
        }
    }
    return a.reduce(previousValue,initialValue);
}

const max_func = (a) => {
    if (a. length>0) {
        return Math.max(...a)
        }
    else{
        return null;
    }
}

const min_func = (a) => {
    if (a. length>0) {
        return Math.min(...a)
        }
    else{
        return null;
    }
}

//module.exports = {add, head_func};

module.exports = {add, head_func,tail_func, max_func, min_func, map_func, filter_func, reduce_func};
